#  Basic mission with Drone simulator and RViz

In order to install and execute this project, perform the following steps.

### Install the project

- Requirements: Linux Ubuntu 18.04 with ROS Melodic or Linux Ubuntu 16.04 with ROS Kinetic.

- Download the installation files:

        $ git clone https://bitbucket.org/visionaerialrobotics/aerostack_installers.git ~/temp

- Run the following installation script to install the project "behavior_viewer_test":

        $ ~/temp/install_project_from_source.sh projects_cvar/behavior_viewer_test

### Execute the project

- Open a new terminal and launch roscore to start the ROS master node:

        $ roscore

- Change directory to this project:

        $ cd $AEROSTACK_STACK/projects_test/behavior_viewer_test

- Execute the script that launches the Aerostack components for this project:
 
        $ ./main_launcher.sh

- Wait until the following window is presented:

![RVIz.png](https://i.ibb.co/j574SKP/Captura-de-pantalla-de-2019-06-19-11-12-58.png)

- Execute the following command to run the mission:

        $ rosservice call /drone107/python_based_mission_interpreter_process/start


- To stop the processes execute the following script:

        $ ./stop.sh

- To close the inactive terminals:

        $ killall bash
