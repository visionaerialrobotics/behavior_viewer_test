#!/usr/bin/env python2

import mission_execution_control as mxc
import rospy
'''
This is a simple mission, the drone takes off, follows a path and lands
1.startTask. Starts the task and continues.
2.executeTask. Starts the task and waits until it stops.
'''
def mission():
  print("Starting mission...")

  print("Taking off...")
  mxc.executeTask('TAKE_OFF')

  print("Adding some believes")
  mxc.addBelief('temperature(air, 20)')
  result = mxc.trueBelief('temperature(air, 20)')
  if result==True:
   print("ROTATE...")
   mxc.executeTask('ROTATE', relative_angle = 270)	

  print("FOLLOW_PATH...")
  mxc.executeTask('FOLLOW_PATH', path = [ [0, 2, 1] , [2, 2, 1] , [2, 0, 1] , [0, 0, 1] ])
  #print("Inform operator")
  #api.executeBehavior('INFORM_OPERATOR', message="The drone is going to do an unification and then i is going to land")
  mxc.executeTask('FOLLOW_PATH', path = [ [5,0,3] ,[5,2,3] , [5,2,7] ,[3,2,7] ])
  #print("FOLLOW_PATH...")
  #api.executeBehavior('FOLLOW_PATH', path = [ [0, 9, 0] , [5, 9, 0] , [5, 4, 0] , [5, 4, 4] ])
  success, unification = mxc.queryBelief('position(?X, ?Y)')
  if success==True:
   print(unification['Y'])
   mxc.executeTask('ROTATE', relative_angle = 90)	
  print("LAND...")
  mxc.executeTask('LAND')
  print('Finish mission...')

